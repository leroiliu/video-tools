<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Interfaces;

interface IVideo
{

    /**
     * @param string $url
     * @return array
     */
    public function start(string $url): array;

}
