<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Tools;

use Leroi\VideoTools\Interfaces\IVideo;

class QQVideo extends Base implements IVideo
{


    public function start(string $url): array
    {
        $this->make();
        $this->logic->setOriginalUrl($url);
        $this->logic->checkUrlHasTrue();
        $this->logic->setVid();
        $this->logic->setContents();
        return $this->exportData();
    }

}
