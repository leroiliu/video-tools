<?php


namespace Leroi\VideoTools\Logic;


use Leroi\VideoTools\Enumerates\UserGentType;
use Leroi\VideoTools\Utils\CommonUtil;

class QuanMinLogic extends Base
{

    private $contents;

    public function setContents()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->url,['headers'=>[
            'Connection'=>'keep-alive',
            'User-Agent'=>UserGentType::ANDROID_USER_AGENT
        ]]);
        $body = $response->getBody();
        if ($body instanceof \GuzzleHttp\Psr7\Stream) {
            $body = $body->getContents();
        }
        $contents = htmlspecialchars_decode($body);
        $contents = str_replace(' ','',$contents);
        $this->contents = $contents;
    }

    /**
     * @return mixed
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        $pattern = '/\<metaproperty\=\"og\:url\"content=\"(https?\:\/\/\S+)\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }

    public function getVideoUrl()
    {
        $pattern = '/\<metaproperty\=\"og\:videosrc\"content=\"(https?\:\/\/\S+)\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }

    public function getVideoImage()
    {
        $pattern = '/\<metaproperty\=\"og\:image\"content=\"(https?\:\/\/\S+)\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }

    public function getVideoDesc()
    {
        $pattern = '/\<metaproperty\=\"og\:title\"content=\"(\S+)\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }

    public function getUsername()
    {
        $pattern = '/\<metaproperty\=\"og\:description\"content=\"本视频由(\S+)提供\S+\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }

    public function getUserPic()
    {
        $pattern = '/\<linkrel\=\"shortcuticon\"href\=\"(https?\:\/\/\S+)\"type\=\"image\/x\-icon\"\/?\>/iU';
        preg_match($pattern, $this->contents, $match);
        return  CommonUtil::getData($match[1]);
    }



}
