<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Logic;


class WeibocdnLogic extends Base
{

    public function setMid()
    {
        return '';
    }

    public function setFid()
    {
        return '';
    }

    public function setContents()
    {
        return '';
    }

    private function getContents()
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getFid()
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getMid()
    {
        return '';
    }


    public function getUrl()
    {
        return $this->url;
    }

    public function getVideoUrl()
    {
        return $this->url;
    }

    public function getVideoImage()
    {
        return '';
    }

    public function getVideoDesc()
    {
        return '';
    }

    public function getUsername()
    {
        return '';
    }

    public function getUserPic()
    {
        return '';
    }

}
