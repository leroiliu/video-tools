<?php


namespace Leroi\VideoTools;


class VideoTools
{

    public static function Start($url){
        try {
            try {
                $url = self::str_parse_url($url);
            }catch (\Exception $exception){
                return json_decode($exception->getMessage(),true);
            }
            $validators = include __DIR__ . '/../config/url-validator.php';
            foreach ($validators as $app => $check_urls){
                foreach ($check_urls as $check_url){
                    if (strpos($url, $check_url)>-1){
                        $data = self::DoStart($app,$url);
                        $video_url = isset($data['video_url'])?$data['video_url']:'';
                        if (preg_match('/^(https?\:)?\/\//',$video_url,$match)){
                            if ($match[0]=='//'){
                                $data['video_url'] = 'http:'.$video_url;
                            }
                            return $data;
                        }
                        return self::exception_error('链接解析失败');
                    }
                }
            }
            return self::exception_error('验证没有收录该平台');
        }catch (\Exception $exception){
            return json_decode($exception->getMessage(),true);
        }
    }

    private static function DoStart($app,$url){
        try {
            if ($app=='bili'){
//            VideoManager::Bili()->setUrl("https://b23.tv/3uPg39")->setQuality(BiliQualityType::LEVEL_6)->execution();
//            VideoManager::Bili()->start("https://b23.tv/3uPg39");
                return VideoManager::Bili()->start($url);
            }elseif ($app=='douyin'){
//            VideoManager::DouYin()->setIsCheckUrl(true)->setUrlValidator(['douyin.com', 'iesdouyin.com'])->start("https://v.douyin.com/JtWeABy/");
                return VideoManager::DouYin()->start($url);
            }elseif ($app=='huoshan'){
//            VideoManager::HuoShan()->start("https://share.huoshan.com/hotsoon/s/sY127kucVe8/");
                return VideoManager::HuoShan()->start($url);
            }elseif ($app=='kuaishou'){
                $cookie = 'did=web_e19cfe96787746a5b4311f56acaf5be4; didv=1593660908000; clientid='.mt_rand(0,100).'; client_key=65890b29; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1594966818,1595649199; sid=6281151b7052a88cfaba3a43';
//            VideoManager::KuaiShou()->setCookie($cookie)->start("https://v.kuaishou.com/7cHafy");
                return VideoManager::KuaiShou()->setCookie($cookie)->start($url);
            }elseif ($app=='livideo'){
//            VideoManager::LiVideo()->start("https://www.pearvideo.com/detail_1715571?st=7");
                return VideoManager::LiVideo()->start($url);
            }elseif ($app=='meipai'){
//            VideoManager::MeiPai()->start("http://www.meipai.com/media/6753249893463913057?client_id=1089857302&utm_media_id=6753249893463913057&utm_source=meipai_share&utm_term=meipai_android&gid=&utm_content=9457&utm_share_type=3")
                return VideoManager::MeiPai()->start($url);
            }elseif ($app=='momo'){
//            VideoManager::MoMo()->start("https://m.immomo.com/s/moment/new-share-v2/au9576177907.html?time=1610452962&name=YF/Xvtvwm065VLOLT2Pq7w==&avatar=E5D52353-A65B-57A7-C4C8-F6DA47893C7420210111&isdaren=0&isuploader=0&from=qqfriend");
                return VideoManager::MoMo()->start($url);
            }elseif ($app=='pipigaoxiao'){
//            VideoManager::PiPiGaoXiao()->start("https://h5.pipigx.com/pp/post/400775354154?zy_to=copy_link&share_count=1&m=3fa43ba21a186764190eff0ebb55802a&app=&type=post&did=4188b1b0b7a299f8&mid=2586040233659&pid=400775354154");
                return VideoManager::PiPiGaoXiao()->start($url);
            }elseif ($app=='pipixia'){
//            VideoManager::PiPiXia()->start("https://h5.pipix.com/s/wkwJBk/");
                return VideoManager::PiPiXia()->start($url);
            }elseif ($app=='quanminggaoxiao'){
                //找不到这个APP---------找不到这个APP---------找不到这个APP---------找不到这个APP---------找不到这个APP---------找不到这个APP---------找不到这个APP
                return VideoManager::QuanMingGaoXiao()->start($url);
            }elseif ($app=='shuabao'){
//            VideoManager::ShuaBao()->start("http://h5.shua8cn.com/video_share?share_type=copy_url&video_source=recommend&platform=android&show_id=380b84faf9fabbcc7cf0de768d111e81&uid=0&invite_code=&_timestamp=1588001308&_sign=3009fbd584b721e2b7e46f3e94452d40");
                return VideoManager::ShuaBao()->start($url);
            }elseif ($app=='toutiao'){
//            VideoManager::TouTiao()->start("https://m.toutiaoimg.cn/a6911594787594306060/?app=news_article_lite&is_hit_share_recommend=0&share_token=c1d74b43-1b8a-461b-9564-c71457da4579");
                return VideoManager::TouTiao()->start($url);
            }elseif ($app=='weishi'){
//            VideoManager::WeiShi()->start("https://h5.weishi.qq.com/weishi/feed/6Z7Uxwu7H1JsBFXPo/wsfeed?wxplay=1&id=6Z7Uxwu7H1JsBFXPo&collectionid=1bc8fe60a09dce07a4c5ce449b3c16bf&spid=8404838818534879236&qua=v1_and_weishi_6.7.6_588_312027000_d&chid=100081003&pkg=&attach=cp_reserves3_1000370721");
                return VideoManager::WeiShi()->start($url);
            }elseif ($app=='xiaokaxiu'){
//            VideoManager::XiaoKaXiu()->start("https://mobile.xiaokaxiu.com/video?id=6552134443723661312");
                return VideoManager::XiaoKaXiu()->start($url);
            }elseif ($app=='xigua'){
//            VideoManager::XiGua()->start("https://v.ixigua.com/JnL31XY/");
                return VideoManager::XiGua()->start($url);
            }elseif ($app=='zuiyou'){
//              $url = 'https://share.izuiyou.com/hybrid/share/post?pid=206001052&zy_to=applink&share_count=1&m=93339a112689624c2fc205bc6c3057d8&d=99d0cb1b24bbae0460dc014175e1f105dfe0d8310234a84ca71389fa361ba2ad&app=zuiyou&recommend=r0&name=n0&title_type=t0';
                return VideoManager::ZuiYou()->start($url);
            }elseif ($app=='weibocdn'){
                return VideoManager::WeiBo()->weibocdnStart($url);
            }elseif ($app=='newweibo'){
                return VideoManager::WeiBo()->newVideoStart($url);
            }elseif ($app=='weibo'){
//            VideoManager::WeiBo()->setIsCheckUrl(false)->start("http://f.video.weibocdn.com/Eotpb2Xhlx07JcNpvYGc010412008iZK0E010.mp4?label=mp4_720p&template=720x1280.24.0&trans_finger=c3f00996be5378650057cf237d7bfffd&ori=0&ps=1A1eh1m4ElLYfp&Expires=1610457865&ssig=QcRgMQzRKG&KID=unistore,video");
                return VideoManager::WeiBo()->start($url);
            }elseif ($app=='miaopai'){
//            VideoManager::MiaoPai()->start("http://n.miaopai.com/media/iV5lNBsA4tXHGYlpPq2TewkhKC-uV2Kt.htm");
                return VideoManager::MiaoPai()->start($url);
            }elseif ($app=='qqvideo'){
//            VideoManager::QQVideo()->start("http://m.v.qq.com/play.html?cid=mzc00200hbhxpv5&vid=r0035ew44rn&vuid24=L1thTzb39YCQ0DYveZADAA%3D%3D&url_from=share&second_share=0&share_from=copy&pgid=page_detail&mod_id=mod_toolbar");
                return VideoManager::QQVideo()->start($url);
            }elseif ($app=='taobao'){
//            VideoManager::TaoBao()->start("https://m.tb.cn/h.4iv8cuX?sm=4aeb53");
                return VideoManager::TaoBao()->start($url);
            }elseif ($app=='quanmin'){
//            VideoManager::QuanMin()->start('https://quanmin.baidu.com/sv?source=share-h5&pd=qm_share_mvideo&vid=3818565122102433212&shared_cuid=AnR5B');
                return VideoManager::QuanMin()->start($url);
            }elseif ($app=='haokan'){
//            VideoManager::HaoKan()->start('https://baijiahao.baidu.com/s?id=1688468208908088628&wfr=content');
                return VideoManager::HaoKan()->start($url);
            }else{
                return self::exception_error('暂不支持该链接的解析');
            }
        }catch (\Exception $exception){
            return json_decode($exception->getMessage(),true);
        }
    }

    private static function str_parse_url($str){
        if (preg_match('/https?:\/\/[^\s]*/',$str,$match)){
            return $match[0];
        }
        return self::exception_error('未检测到链接');
    }

    private static function exception_error($message='失败',$data=[],$code=500){
        throw new \Exception(json_encode(['code'=>500,'message'=>$message,'data'=>$data],JSON_UNESCAPED_UNICODE));
    }

}
