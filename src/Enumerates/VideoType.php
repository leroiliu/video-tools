<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Enumerates;

class VideoType
{

    const VIDEO = "video";
    const IMAGE = "image";
    const OTHER = "other";


}
