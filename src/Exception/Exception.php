<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Exception;

use Throwable;

class Exception extends \Exception
{


    const INVALID_MANAGER_CODE = 550;
    const ERROR_VIDEO_CODE = 551;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
