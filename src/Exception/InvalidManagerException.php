<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Exception;


class InvalidManagerException extends Exception
{

    public function __construct($message = "")
    {
        parent::__construct("InvalidManager : " . $message, self::INVALID_MANAGER_CODE, null);
    }

}
