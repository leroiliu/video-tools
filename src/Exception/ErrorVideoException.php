<?php
declare (strict_types=1);

namespace Leroi\VideoTools\Exception;

class ErrorVideoException extends Exception
{

    public function __construct($message = "")
    {
        parent::__construct("ErrorVideo : " . $message, self::ERROR_VIDEO_CODE, null);
    }

}
