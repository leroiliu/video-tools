<?php
declare (strict_types=1);

namespace Leroi\VideoTools;

use Leroi\VideoTools\Exception\InvalidManagerException;
use Leroi\VideoTools\Interfaces\IVideo;
use Leroi\VideoTools\Tools\Bili;
use Leroi\VideoTools\Tools\DouYin;
use Leroi\VideoTools\Tools\HuoShan;
use Leroi\VideoTools\Tools\KuaiShou;
use Leroi\VideoTools\Tools\LiVideo;
use Leroi\VideoTools\Tools\MeiPai;
use Leroi\VideoTools\Tools\MiaoPai;
use Leroi\VideoTools\Tools\MoMo;
use Leroi\VideoTools\Tools\PiPiGaoXiao;
use Leroi\VideoTools\Tools\PiPiXia;
use Leroi\VideoTools\Tools\QQVideo;
use Leroi\VideoTools\Tools\QuanMingGaoXiao;
use Leroi\VideoTools\Tools\ShuaBao;
use Leroi\VideoTools\Tools\TaoBao;
use Leroi\VideoTools\Tools\TouTiao;
use Leroi\VideoTools\Tools\WeiBo;
use Leroi\VideoTools\Tools\WeiShi;
use Leroi\VideoTools\Tools\XiaoKaXiu;
use Leroi\VideoTools\Tools\XiGua;
use Leroi\VideoTools\Tools\ZuiYou;
use Leroi\VideoTools\Tools\QuanMin;
use Leroi\VideoTools\Tools\HaoKan;


/**
 * @method static HuoShan HuoShan(...$params)
 * @method static DouYin DouYin(...$params)
 * @method static KuaiShou KuaiShou(...$params)
 * @method static TouTiao TouTiao(...$params)
 * @method static XiGua XiGua(...$params)
 * @method static WeiShi WeiShi(...$params)
 * @method static PiPiXia PiPiXia(...$params)
 * @method static ZuiYou ZuiYou(...$params)
 * @method static MeiPai MeiPai(...$params)
 * @method static LiVideo LiVideo(...$params)
 * @method static QuanMingGaoXiao QuanMingGaoXiao(...$params)
 * @method static PiPiGaoXiao PiPiGaoXiao(...$params)
 * @method static MoMo MoMo(...$params)
 * @method static ShuaBao ShuaBao(...$params)
 * @method static XiaoKaXiu XiaoKaXiu(...$params)
 * @method static Bili Bili(...$params)
 * @method static WeiBo WeiBo(...$params)
 * @method static MiaoPai MiaoPai(...$params)
 * @method static QQVideo QQVideo(...$params)
 * @method static TaoBao TaoBao(...$params)
 * @method static QuanMin QuanMin(...$params)
 * @method static HaoKan HaoKan(...$params)
 */
class VideoManager
{

    public function __construct()
    {
    }

    /**
     * @param $method
     * @param $params
     * @return mixed
     */
    public static function __callStatic($method, $params)
    {
        $app = new self();
        return $app->create($method, $params);
    }

    /**
     * @param string $method
     * @param array $params
     * @return mixed
     * @throws InvalidManagerException
     */
    private function create(string $method, array $params)
    {
        $className = __NAMESPACE__ . '\\Tools\\' . $method;
        if (!class_exists($className)) {
            throw new InvalidManagerException("the method name does not exist . method : {$method}");
        }
        return $this->make($className, $params);
    }

    /**
     * @param string $className
     * @param array $params
     * @return mixed
     * @throws InvalidManagerException
     */
    private function make(string $className, array $params)
    {
        $app = new $className($params);
        if ($app instanceof IVideo) {
            return $app;
        }
        throw new InvalidManagerException("this method does not integrate IVideo . namespace : {$className}");
    }
}
